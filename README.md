# Basketball Team Manager

## Requeriments

- PHP 7.1
- Composer
- Docker
- Docker Compose
- MySQL (Included in docker-compose)

## Initial setup

```
$ git clone https://gitlab.com/ulisesserranomartinez/basketball-team-manager.git
$ cd basketball-team-manager
$ docker-compose up
$ php bin/console doctrine:migrations:migrate
```
