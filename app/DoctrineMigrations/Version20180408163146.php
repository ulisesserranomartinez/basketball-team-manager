<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180408163146 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE player (id INT AUTO_INCREMENT NOT NULL, number INT NOT NULL, name LONGTEXT NOT NULL, role enum(\'BASE\', \'ESCOLTA\', \'ALERO\', \'ALA-PIVOT\', \'PIVOT\'), score INT NOT NULL, UNIQUE INDEX UNIQ_98197A6596901F54 (number), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tactic (id INT AUTO_INCREMENT NOT NULL, name LONGTEXT NOT NULL, base_tactic TINYINT(1) NOT NULL, role1 enum(\'BASE\', \'ESCOLTA\', \'ALERO\', \'ALA-PIVOT\', \'PIVOT\'), role2 enum(\'BASE\', \'ESCOLTA\', \'ALERO\', \'ALA-PIVOT\', \'PIVOT\'), role3 enum(\'BASE\', \'ESCOLTA\', \'ALERO\', \'ALA-PIVOT\', \'PIVOT\'), role4 enum(\'BASE\', \'ESCOLTA\', \'ALERO\', \'ALA-PIVOT\', \'PIVOT\'), role5 enum(\'BASE\', \'ESCOLTA\', \'ALERO\', \'ALA-PIVOT\', \'PIVOT\'), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE operating_history (id INT AUTO_INCREMENT NOT NULL, entity LONGTEXT NOT NULL, operation LONGTEXT NOT NULL, date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE tactic');
        $this->addSql('DROP TABLE operating_history');
    }
}
