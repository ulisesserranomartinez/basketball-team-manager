<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180408165757 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql(
            'INSERT INTO tactic (name,base_tactic,role1,role2,role3,role4,role5)'.
                ' VALUES (\'Defensa 1-3-1\',true,\'BASE\',\'ESCOLTA\',\'ESCOLTA\',\'ALA-PIVOT\',\'PIVOT\'),'.
                '(\'Defensa Zonal 2-3\',true,\'BASE\',\'BASE\',\'ALERO\',\'PIVOT\',\'ALA-PIVOT\'),'.
                '(\'Ataque 2-2-1\',true,\'BASE\',\'ALERO\',\'ESCOLTA\',\'PIVOT\',\'ALA-PIVOT\');'
        );
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM tactic');
    }
}
