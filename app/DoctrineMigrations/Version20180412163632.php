<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180412163632 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE operating_history ADD serialized_entity LONGTEXT NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tactic CHANGE role1 role1 VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE role2 role2 VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE role3 role3 VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE role4 role4 VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE role5 role5 VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
