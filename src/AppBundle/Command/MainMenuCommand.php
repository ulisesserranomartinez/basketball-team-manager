<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Command;

use AppBundle\Util\ConsoleOutputUtil;
use AppBundle\Util\MainMenuOptionsEnum;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class MainMenuCommand
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class MainMenuCommand extends Command
{

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var SymfonyStyle $io */
    private $io;

    /**
     * Configure console command parameters.
     */
    protected function configure(){
        $this->setName('basketball-team-manager:main-menu')
            ->setDescription('Comando para mostrar el menu principal.');
    }

    /**
     * Entry point of console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        $this->showMainMenu();
    }

    /**
     * Show header and ask for menu choice
     */
    private function showMainMenu()
    {
        $this->showHeader();

        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            '¿Que acción desea realizar?',
            MainMenuOptionsEnum::toArrayValues()
        );
        $question->setErrorMessage('La opción %s es invalida.');
        $choice = $helper->ask($this->input, $this->output, $question);

        $this->showSelectedOption($choice);
    }

    /**
     * Retrieve selected option by user and execute the command
     *
     * @param $choice
     */
    private function showSelectedOption($choice)
    {
        switch ($choice) {
            case MainMenuOptionsEnum::PLAYER_MENU:
                $this->showPlayerMenu();
                break;
            case MainMenuOptionsEnum::TACTIC_MENU:
                $this->showTacticMenu();
                break;
            case MainMenuOptionsEnum::TEAM_ALIGNMENT_MENU:
                $this->showTeamAlignmentCalculatorMenu();
                break;
            default:
                break;
        }
    }

    /**
     * Show player menu command
     */
    private function showPlayerMenu()
    {
        $command = $this->getApplication()->find('basketball-team-manager:player-menu');
        $arguments = ['command' => 'basketball-team-manager:player-menu'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->output);

        $this->showMainMenu();
    }

    /**
     * Show tactic menu command
     */
    private function showTacticMenu()
    {
        $command = $this->getApplication()->find('basketball-team-manager:tactic-menu');
        $arguments = ['command' => 'basketball-team-manager:tactic-menu'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->output);

        $this->showMainMenu();
    }

    /**
     * Show team alignment menu command
     */
    private function showTeamAlignmentCalculatorMenu()
    {
        $command = $this->getApplication()->find('basketball-team-manager:team-alignment-calculator');
        $arguments = ['command' => 'basketball-team-manager:team-alignment-calculator'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->output);

        $this->showMainMenu();
    }

    /**
     * Show header command in output console.
     */
    private function showHeader()
    {
        ConsoleOutputUtil::cleanConsole($this->output);

        $this->io->title('Welcome to Basketball Team Manager');
    }

}
