<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Command;

use AppBundle\Service\PlayerService;
use AppBundle\Util\ConsoleOutputUtil;
use AppBundle\Util\PlayerMenuOptionsEnum;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class PlayerCommand
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class PlayerCommand extends Command
{

    /** @var PlayerService $playerService */
    private $playerService;

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var SymfonyStyle $io */
    private $io;

    /**
     * PlayerCommand constructor.
     *
     * @param PlayerService $playerService
     */
    public function __construct(PlayerService $playerService)
    {
        $this->playerService = $playerService;

        parent::__construct();
    }

    /**
     * Configure console command parameters.
     */
    protected function configure(){
        $this->setName('basketball-team-manager:player-menu')
            ->setDescription('Comando para mostrar el menu de jugadores.');
    }

    /**
     * Entry point of console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        $this->showPlayerMenu();
    }

    /**
     * Show header and ask for menu choice
     */
    private function showPlayerMenu()
    {
        $this->showHeader();

        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            '¿Que acción desea realizar?',
            PlayerMenuOptionsEnum::toArrayValues()
        );
        $question->setErrorMessage('La opción %s es invalida.');
        $choice = $helper->ask($this->input, $this->output, $question);

        $this->showSelectedOption($choice);
    }

    /**
     * Retrieve selected option by user and execute the command
     *
     * @param $choice
     */
    private function showSelectedOption($choice)
    {
        switch ($choice) {
            case PlayerMenuOptionsEnum::ADD_PLAYER_MENU:
                $this->showPlayerCreateMenu();
                break;
            case PlayerMenuOptionsEnum::LIST_PLAYER_MENU:
                $this->showPlayerListMenu();
                break;
            case PlayerMenuOptionsEnum::DELETE_PLAYER_MENU:
                $this->showPlayerDeleteMenu();
                break;
            default:
                break;
        }
    }

    /**
     * Show player create command menu
     */
    private function showPlayerCreateMenu()
    {
        $command = $this->getApplication()->find('basketball-team-manager:player-create');
        $arguments = ['command' => 'basketball-team-manager:player-create'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->output);

        $this->showPlayerMenu();
    }

    /**
     * Show player delete command menu
     */
    private function showPlayerDeleteMenu()
    {
        $command = $this->getApplication()->find('basketball-team-manager:player-delete');
        $arguments = ['command' => 'basketball-team-manager:player-delete'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->output);

        $this->showPlayerMenu();
    }

    /**
     * Show player list command menu
     */
    private function showPlayerListMenu()
    {
        $command = $this->getApplication()->find('basketball-team-manager:player-list');
        $arguments = ['command' => 'basketball-team-manager:player-list'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->output);

        $this->showPlayerMenu();
    }

    /**
     * Show header command in output console.
     */
    private function showHeader()
    {
        ConsoleOutputUtil::cleanConsole($this->output);

        $this->io->title('Basketball Team Manager - Menu jugador');
    }

}
