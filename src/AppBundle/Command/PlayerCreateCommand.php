<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Command;

use AppBundle\Entity\Player;
use AppBundle\Service\PlayerService;
use AppBundle\Util\ConsoleOutputUtil;
use AppBundle\Util\Roles;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class PlayerCreateCommand
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class PlayerCreateCommand extends Command
{

    /** @var PlayerService $playerService */
    private $playerService;

    /** @var QuestionHelper $questionHelper */
    private $questionHelper;

    /** @var InputInterface $input */
    private $input;

    /** @var OutputInterface $output */
    private $output;

    /** @var SymfonyStyle $io */
    private $io;

    /**
     * PlayerCommand constructor.
     *
     * @param PlayerService $playerService
     */
    public function __construct(PlayerService $playerService)
    {
        $this->playerService = $playerService;

        parent::__construct();
    }

    /**
     * Configure console command parameters.
     */
    protected function configure()
    {
        $this
            ->setName('basketball-team-manager:player-create')
            ->setDescription('Comando para añadir un jugador');
    }

    /**
     * Entry point of console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->questionHelper = $this->getHelper('question');
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        $this->questionPlayer();
    }

    /**
     * Show player related questions to retrieve data
     */
    private function questionPlayer()
    {
        $this->showHeader();

        $playerName = $this->questionName();
        $playerNumber = $this->questionNumber();
        $playerRole = $this->questionRole();
        $playerScore = $this->questionScore();

        $player = new Player();
        $player
            ->setName($playerName)
            ->setNumber($playerNumber)
            ->setRole($playerRole)
            ->setScore($playerScore);

        $this->playerService->createPlayer($player);
    }

    /**
     * Show name player question in output console.
     *
     * @return string
     */
    private function questionName()
    {
        $question = new Question('Indique el nombre del jugador: ');
        $question->setValidator(function ($answer) {
            if (trim($answer) == '') {
                throw new \Exception('No puede indicar un nombre de jugador vacío.');
            }

            return $answer;
        });
        $playerName = $this->questionHelper->ask($this->input, $this->output, $question);

        return $playerName;
    }

    /**
     * Show number player question in output console.
     *
     * @return int
     */
    private function questionNumber()
    {
        $question = new Question('Indique el dorsal del jugador: ');

        $question->setValidator(function ($answer) {
            if (!is_numeric($answer)) {
                throw new \RuntimeException(
                    'Se debe introducir un número entero.'
                );
            }

            if ($this->playerService->findPlayerByNumber($answer)) {
                throw new \RuntimeException(
                    'El número de dorsal introducido ya existe.'
                );
            }

            return $answer;
        });

        $playerNumber = $this->questionHelper->ask($this->input, $this->output, $question);

        return $playerNumber;
    }

    /**
     * Show role player question in output console.
     *
     * @return string
     */
    private function questionRole()
    {
        $question = new ChoiceQuestion(
            '¿Cual es el rol del jugador?',
            Roles::toArrayValues()
        );
        $question->setErrorMessage('La opción %s es invalida.');

        $playerRole = $this->questionHelper->ask($this->input, $this->output, $question);

        return $playerRole;
    }

    /**
     * Show score player question in output console.
     *
     * @return int
     */
    private function questionScore()
    {
        $question = new Question('Indique la puntuación del jugador: ');
        $question->setValidator(function ($answer) {
            if (!is_numeric($answer)) {
                throw new \RuntimeException('Se debe introducir un número entero.');
            }

            if ($answer < 0 || $answer > 100) {
                throw new \RuntimeException('El valor introducido debe estar entre 0 y 100.');
            }

            return $answer;
        });
        $playerScore = $this->questionHelper->ask($this->input, $this->output, $question);

        return $playerScore;
    }

    /**
     * Show header command in output console.
     */
    private function showHeader()
    {
        ConsoleOutputUtil::cleanConsole($this->output);

        $this->io->title('Basketball Team Manager - Menu jugador - Crear jugador');
    }

}
