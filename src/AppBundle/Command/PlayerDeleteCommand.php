<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Command;

use AppBundle\Entity\Player;
use AppBundle\Exception\PlayerNotFoundException;
use AppBundle\Service\PlayerService;
use AppBundle\Util\ConsoleOutputUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class PlayerDeleteCommand
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class PlayerDeleteCommand extends Command
{

    /** @var PlayerService $playerService */
    private $playerService;

    /** @var QuestionHelper $questionHelper */
    private $questionHelper;

    /** @var InputInterface $input */
    private $input;

    /** @var OutputInterface $output */
    private $output;

    /** @var SymfonyStyle $io */
    private $io;

    /**
     * PlayerCommand constructor.
     *
     * @param PlayerService $playerService
     */
    public function __construct(PlayerService $playerService)
    {
        $this->playerService = $playerService;

        parent::__construct();
    }

    /**
     * Configure console command parameters.
     */
    protected function configure()
    {
        $this
            ->setName('basketball-team-manager:player-delete')
            ->setDescription('Comando para eliminar un jugador');
    }

    /**
     * Entry point of console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->questionHelper = $this->getHelper('question');
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        $this->showHeader();
        $this->playerList();
        $this->questionNumber();
    }

    /**
     * Ask for player number that will be deleted
     *
     * @return mixed
     */
    private function questionNumber()
    {
        $question = new Question('Indique el dorsal del jugador que desea eliminar: ');
        $question->setValidator(function ($answer) {
            if (!is_numeric($answer)) {
                throw new \RuntimeException('Se debe introducir un número entero.');
            }

            try {
                $this->playerService->deletePlayerByNumber($answer);
            } catch (PlayerNotFoundException $ex) {
                throw new \RuntimeException('El jugador con el dorsal seleccionado no existe.');
            }

            return $answer;
        });

        $playerNumber = $this->questionHelper->ask($this->input, $this->output, $question);

        $this->showDeleteConfirmation($playerNumber);
    }

    /**
     * Show delete confirmation success message
     *
     * @param int $playerNumber
     */
    private function showDeleteConfirmation(int $playerNumber)
    {
        $io = new SymfonyStyle($this->input, $this->output);
        $io->success("Se ha eliminado el jugador con el dorsal número: $playerNumber");
        ConsoleOutputUtil::pressAnyKeyToContinue($this->input, $this->output, $this->questionHelper);
    }

    /**
     * Show player list in output with Symfony style table
     */
    private function playerList()
    {
        $io = new SymfonyStyle($this->input, $this->output);

        $playerList = $this->playerService->listPlayer();

        $compositionList = [];

        /** @var Player $player */
        foreach ($playerList as $player) {
            $compositionList[] = [$player->getName(), $player->getNumber()];
        }

        $io->table(
            ['Nombre del jugador', 'Dorsal'],
            $compositionList
        );
    }

    /**
     * Show header command in output console.
     */
    private function showHeader()
    {
        ConsoleOutputUtil::cleanConsole($this->output);

        $this->io->title('Basketball Team Manager - Menu jugador - Eliminar jugador');
    }

}
