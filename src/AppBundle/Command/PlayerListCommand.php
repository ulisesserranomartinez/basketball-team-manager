<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Command;

use AppBundle\Entity\Player;
use AppBundle\Service\PlayerService;
use AppBundle\Util\ConsoleOutputUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class PlayerListCommand
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class PlayerListCommand extends Command
{

    /** @var PlayerService $playerService */
    private $playerService;

    /** @var QuestionHelper $questionHelper */
    private $questionHelper;

    /** @var InputInterface $input */
    private $input;

    /** @var OutputInterface $output */
    private $output;

    /** @var SymfonyStyle $io */
    private $io;

    /**
     * PlayerCommand constructor.
     *
     * @param PlayerService $playerService
     */
    public function __construct(PlayerService $playerService)
    {
        $this->playerService = $playerService;

        parent::__construct();
    }

    /**
     * Configure console command parameters.
     */
    protected function configure()
    {
        $this
            ->setName('basketball-team-manager:player-list')
            ->setDescription('Comando para listar jugadores');
    }

    /**
     * Entry point of console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->questionHelper = $this->getHelper('question');
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        $this->showHeader();
        $order = $this->playerOrder();
        $this->playerList($order);
        ConsoleOutputUtil::pressAnyKeyToContinue($input, $output, $this->questionHelper);
    }

    /**
     * Ask for player list order
     *
     * @return array
     */
    private function playerOrder()
    {
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            '¿Por que columna desea ordenar el listado?',
            array('Sin orden', 'Dorsal', 'Puesto y valoración media'),
            0
        );
        $question->setErrorMessage('La opción %s es invalida.');
        $choice = $helper->ask($this->input, $this->output, $question);

        $order = [];

        switch ($choice) {
            case 'Dorsal':
                $order = ['number' => 'ASC'];
                break;
            case 'Puesto y valoración media':
                $order = ['role' => 'ASC', 'score' => 'DESC'];
                break;
        }

        return $order;
    }

    /**
     * Show player list in output with Symfony style table
     *
     * @param array $orderBy
     */
    private function playerList(array $orderBy = [])
    {
        $io = new SymfonyStyle($this->input, $this->output);

        $playerList = $this->playerService->listPlayer($orderBy);

        $compositionList = [];

        /** @var Player $player */
        foreach ($playerList as $player) {
            $compositionList[] = [
                $player->getId(),
                $player->getName(),
                $player->getNumber(),
                $player->getRole(),
                $player->getScore()
            ];
        }

        $io->table(
            ['Id', 'Nombre del jugador', 'Dorsal', 'Role', 'Puntuación'],
            $compositionList
        );
    }

    /**
     * Show header command in output console.
     */
    private function showHeader()
    {
        ConsoleOutputUtil::cleanConsole($this->output);

        $this->io->title('Basketball Team Manager - Menu jugador - Listar jugadores');
    }

}
