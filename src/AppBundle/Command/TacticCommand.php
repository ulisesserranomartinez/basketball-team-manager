<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Command;

use AppBundle\Util\ConsoleOutputUtil;
use AppBundle\Util\TacticMenuOptionsEnum;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class TacticCommand
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TacticCommand extends Command
{

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var SymfonyStyle $io */
    private $io;

    /**
     * Configure console command parameters.
     */
    protected function configure(){
        $this->setName('basketball-team-manager:tactic-menu')
            ->setDescription('Comando para mostrar el menu de tácticas.');
    }

    /**
     * Entry point of console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        $this->showTacticMenu();
    }

    /**
     * Show header and ask for menu choice
     */
    private function showTacticMenu()
    {
        $this->showHeader();

        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            '¿Que acción desea realizar?',
            TacticMenuOptionsEnum::toArrayValues()
        );
        $question->setErrorMessage('La opción %s es invalida.');
        $choice = $helper->ask($this->input, $this->output, $question);

        $this->showSelectedOption($choice);
    }

    /**
     * Retrieve selected option by user and execute the command
     *
     * @param $choice
     */
    private function showSelectedOption($choice)
    {
        switch ($choice) {
            case TacticMenuOptionsEnum::ADD_TACTIC_MENU:
                $this->showTacticCreateMenu();
                break;
            case TacticMenuOptionsEnum::LIST_TACTIC_MENU:
                $this->showTacticListMenu();
                break;
            case TacticMenuOptionsEnum::DELETE_TACTIC_MENU:
                $this->showTacticDeleteMenu();
                break;
            case 'Cancelar':
                break;
            default:
                $this->output->writeln('Esta opción aún no esta disponible');
                $this->showTacticMenu();
        }
    }

    /**
     * Show tactic create command menu
     */
    private function showTacticCreateMenu()
    {
        $command = $this->getApplication()->find('basketball-team-manager:tactic-create');
        $arguments = ['command' => 'basketball-team-manager:tactic-create'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->output);

        $this->showTacticMenu();
    }

    /**
     * Show tactic list command menu
     */
    private function showTacticListMenu()
    {
        $command = $this->getApplication()->find('basketball-team-manager:tactic-list');
        $arguments = ['command' => 'basketball-team-manager:tactic-list'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->output);

        $this->showTacticMenu();
    }

    /**
     * Show tactic delete command menu
     */
    private function showTacticDeleteMenu()
    {
        $command = $this->getApplication()->find('basketball-team-manager:tactic-delete');
        $arguments = ['command' => 'basketball-team-manager:tactic-delete'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->output);

        $this->showTacticMenu();
    }

    /**
     * Show header command in output console.
     */
    private function showHeader()
    {
        ConsoleOutputUtil::cleanConsole($this->output);

        $this->io->title('Basketball Team Manager - Menu tácticas');
    }

}
