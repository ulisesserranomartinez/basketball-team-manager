<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Command;

use AppBundle\Entity\Tactic;
use AppBundle\Service\TacticService;
use AppBundle\Util\ConsoleOutputUtil;
use AppBundle\Util\Roles;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class TacticCreateCommand
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TacticCreateCommand extends Command
{

    /** @var TacticService $tacticService */
    private $tacticService;

    /** @var QuestionHelper $questionHelper */
    private $questionHelper;

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var SymfonyStyle $io */
    private $io;

    /**
     * TacticCreateCommand constructor.
     *
     * @param TacticService $tacticService
     */
    public function __construct(TacticService $tacticService)
    {
        $this->tacticService = $tacticService;

        parent::__construct();
    }

    /**
     * Configure console command parameters.
     */
    protected function configure(){
        $this->setName('basketball-team-manager:tactic-create')
            ->setDescription('Comando para añadir una táctica.');
    }

    /**
     * Entry point of console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->questionHelper = $this->getHelper('question');
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        $this->showHeader();

        $tacticName = $this->questionName();
        $role1 = $this->questionRole();
        $role2 = $this->questionRole();
        $role3 = $this->questionRole();
        $role4 = $this->questionRole();
        $role5 = $this->questionRole();

        $tactic = new Tactic();
        $tactic
            ->setName($tacticName)
            ->setBaseTactic(false)
            ->setRole1($role1)
            ->setRole2($role2)
            ->setRole3($role3)
            ->setRole4($role4)
            ->setRole5($role5);

        $this->tacticService->createTactic($tactic);
    }

    private function questionName()
    {
        $question = new Question('Indique el nombre de la táctica: ');
        $question->setValidator(function ($answer) {
            if (trim($answer) == '') {
                throw new \Exception('No puede indicar un nombre de táctica vacío.');
            }

            return $answer;
        });
        $tacticName = $this->questionHelper->ask($this->input, $this->output, $question);

        return $tacticName;
    }

    private function questionRole()
    {
        $question = new ChoiceQuestion(
            '¿Role de la posición?',
            Roles::toArrayValues()
        );
        $question->setErrorMessage('La opción %s es invalida.');
        $choice = $this->questionHelper->ask($this->input, $this->output, $question);

        return $choice;
    }

    /**
     * Show header command in output console.
     */
    private function showHeader()
    {
        ConsoleOutputUtil::cleanConsole($this->output);

        $this->io->title('Basketball Team Manager - Menu tácticas - Crear táctica');
    }
}
