<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Command;

use AppBundle\Entity\Tactic;
use AppBundle\Exception\TacticNotCustomizedException;
use AppBundle\Exception\TacticNotFoundException;
use AppBundle\Service\TacticService;
use AppBundle\Util\ConsoleOutputUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class TacticDeleteCommand
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TacticDeleteCommand extends Command
{

    /** @var TacticService $tacticService */
    private $tacticService;

    /** @var QuestionHelper $questionHelper */
    private $questionHelper;

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var SymfonyStyle $io */
    private $io;

    /**
     * TacticCreateCommand constructor.
     *
     * @param TacticService $tacticService
     */
    public function __construct(TacticService $tacticService)
    {
        $this->tacticService = $tacticService;

        parent::__construct();
    }

    /**
     * Configure console command parameters.
     */
    protected function configure(){
        $this->setName('basketball-team-manager:tactic-delete')
            ->setDescription('Comando para eliminar una táctica.');
    }

    /**
     * Entry point of console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->questionHelper = $this->getHelper('question');
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        $this->showHeader();
        $this->tacticList();
        $this->questionId();
    }

    private function questionId()
    {
        $question = new Question('Indique el id de la táctica que desea eliminar: ');
        $question->setValidator(function ($answer) {
            if (!is_numeric($answer)) {
                throw new \RuntimeException(
                    'Se debe introducir un número entero.'
                );
            }

            try {
                $this->tacticService->deleteTactic($answer);
            } catch (TacticNotFoundException $ex) {
                throw new \RuntimeException('No existe la táctica con el id seleccionado.');
            } catch (TacticNotCustomizedException $ex) {
                throw new \RuntimeException('La táctica seleccionada no es una táctica personalizada.');
            }


            return $answer;
        });

        $tacticId = $this->questionHelper->ask($this->input, $this->output, $question);

        $this->showDeleteConfirmation($tacticId);
    }

    /**
     * Show delete confirmation success message
     *
     * @param int $tacticId
     */
    private function showDeleteConfirmation(int $tacticId)
    {
        $io = new SymfonyStyle($this->input, $this->output);
        $io->success("Se ha eliminado táctica con el id: $tacticId");
        ConsoleOutputUtil::pressAnyKeyToContinue($this->input, $this->output, $this->questionHelper);
    }

    /**
     * Show tactic list in output with Symfony style table
     */
    private function tacticList()
    {
        $io = new SymfonyStyle($this->input, $this->output);

        $tacticList = $this->tacticService->listTacticCustomized();

        $compositionList = [];

        /** @var Tactic $tactic */
        foreach ($tacticList as $tactic) {
            $compositionList[] = [$tactic->getId(), $tactic->getName()];
        }

        $io->table(
            ['Id', 'Nombre'],
            $compositionList
        );
    }

    /**
     * Show header command in output console.
     */
    private function showHeader()
    {
        ConsoleOutputUtil::cleanConsole($this->output);

        $this->io->title('Basketball Team Manager - Menu tácticas - Eliminar táctica');
    }

}
