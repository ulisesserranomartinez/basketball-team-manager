<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Command;

use AppBundle\Service\TacticService;
use AppBundle\Util\ConsoleOutputUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class TacticListCommand
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TacticListCommand extends Command
{

    /** @var TacticService $tacticService */
    private $tacticService;

    /** @var SymfonyStyle $io */
    private $io;

    /**
     * TacticCreateCommand constructor.
     *
     * @param TacticService $tacticService
     */
    public function __construct(TacticService $tacticService)
    {
        $this->tacticService = $tacticService;

        parent::__construct();
    }

    /**
     * Configure console command parameters.
     */
    protected function configure(){
        $this->setName('basketball-team-manager:tactic-list')
            ->setDescription('Comando para listar tácticas.');
    }

    /**
     * Entry point of console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $questionHelper = $this->getHelper('question');

        ConsoleOutputUtil::cleanConsole($output);
        $tacticJsonList = $this->tacticService->listTacticAsJson();
        $output->writeln($tacticJsonList);
        ConsoleOutputUtil::pressAnyKeyToContinue($input, $output, $questionHelper);
    }
}
