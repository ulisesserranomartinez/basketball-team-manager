<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Command;

use AppBundle\Entity\Player;
use AppBundle\Entity\Tactic;
use AppBundle\Service\TacticService;
use AppBundle\Service\TeamAlignmentCalculatorService;
use AppBundle\Util\ConsoleOutputUtil;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class TeamAlignmentCalculator
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TeamAlignmentCalculator extends Command
{

    /** @var TacticService $tacticService */
    private $tacticService;

    /** @var TeamAlignmentCalculatorService $teamAlignmentCalculatorService */
    private $teamAlignmentCalculatorService;

    /** @var QuestionHelper $questionHelper */
    private $questionHelper;

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var SymfonyStyle $io */
    private $io;

    /**
     * PlayerCommand constructor.
     *
     * @param TacticService $tacticService
     * @param TeamAlignmentCalculatorService $teamAlignmentCalculatorService
     * @param LoggerInterface $logger
     */
    public function __construct(
        TacticService $tacticService,
        TeamAlignmentCalculatorService $teamAlignmentCalculatorService,
        LoggerInterface $logger
    ){
        $this->tacticService = $tacticService;
        $this->teamAlignmentCalculatorService = $teamAlignmentCalculatorService;

        parent::__construct();
    }

    /**
     * Configure console command parameters.
     */
    protected function configure(){
        $this->setName('basketball-team-manager:team-alignment-calculator')
            ->setDescription('Comando para sugerir la alineación ideal en base a los jugadores disponibles.');
    }

    /**
     * Entry point of console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     *
     * @throws \AppBundle\Exception\TacticCombinationNotPossibleException
     * @throws \AppBundle\Exception\TacticNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->questionHelper = $this->getHelper('question');
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        $this->showHeader();
        $this->showTacticList();
        $tacticId = $this->questionId();
        $this->showCalculatedAlignment($tacticId);
        ConsoleOutputUtil::pressAnyKeyToContinue($this->input, $this->output, $this->questionHelper);
    }

    /**
     * @param int $tacticId
     *
     * @throws \AppBundle\Exception\TacticCombinationNotPossibleException
     * @throws \AppBundle\Exception\TacticNotFoundException
     */
    private function showCalculatedAlignment(int $tacticId)
    {
        /** @var Player[] $playerList */
        $playerList = $this->teamAlignmentCalculatorService->calculateAlignment($tacticId);

        $io = new SymfonyStyle($this->input, $this->output);

        $compositionList = [];

        /** @var Player $player */
        foreach ($playerList as $player) {
            $compositionList[] = [
                $player->getId(),
                $player->getName(),
                $player->getRole(),
                $player->getScore()
            ];
        }

        $io->table(
            ['Id', 'Nombre', 'Rol', 'Puntuación'],
            $compositionList
        );
    }

    /**
     * Ask for tactic id that will be calculated
     *
     * @return mixed
     */
    private function questionId()
    {
        $question = new Question('Indique el id de la táctica que calcular: ');
        $question->setValidator(function ($answer) {
            if (!is_numeric($answer)) {
                throw new \RuntimeException(
                    'Se debe introducir un número entero.'
                );
            }

            $this->tacticService->findTacticById($answer);

            return $answer;
        });

        $tacticId = $this->questionHelper->ask($this->input, $this->output, $question);

        return $tacticId;
    }

    /**
     * Show tactic list in output with Symfony style table
     */
    private function showTacticList()
    {
        $io = new SymfonyStyle($this->input, $this->output);

        $tacticList = $this->tacticService->listTactic();

        $compositionList = [];

        /** @var Tactic $tactic */
        foreach ($tacticList as $tactic) {
            $compositionList[] = [$tactic->getId(), $tactic->getName()];
        }

        $io->table(
            ['Id', 'Nombre'],
            $compositionList
        );
    }

    /**
     * Show header command in output console.
     */
    private function showHeader()
    {
        ConsoleOutputUtil::cleanConsole($this->output);

        $this->io->title('Basketball Team Manager - Menu calculadora de alineaciones');
    }

}
