<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Controller;

use AppBundle\Mapper\PlayerMapper;
use AppBundle\Service\PlayerService;
use AppBundle\Service\ValidationService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class PlayerController
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 *
 * @Route("/api")
 */
class PlayerController extends FOSRestController
{

    /**
     * @var PlayerService $playerService
     */
    private $playerService;

    /**
     * @var PlayerMapper
     */
    private $playerMapper;

    /**
     * @var ValidationService $validationService
     */
    private $validationService;

    /**
     * PlayerController constructor.
     *
     * @param PlayerService $playerService
     * @param PlayerMapper $playerMapper
     * @param ValidationService $validationService
     */
    public function __construct(
        PlayerService $playerService,
        PlayerMapper $playerMapper,
        ValidationService $validationService
    ){
        $this->playerService = $playerService;
        $this->playerMapper = $playerMapper;
        $this->validationService = $validationService;
    }

    /**
     * @Route(
     *      "/player/{playerNumber}",
     *      requirements={
     *          "playerNumber": "\d+"
     *      },
     *      methods={"GET"}
     * )
     *
     * @ApiDoc(
     *      section="Player Section",
     *      description="Get player with specific number.",
     *      output={
     *          "class"="AppBundle\Dto\PlayerDto"
     *      }
     * )
     *
     * @param int $playerNumber
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAction(int $playerNumber)
    {
        $player = $this->playerService->findPlayerByNumber($playerNumber);

        if(!$player) {
            throw new HttpException(400, 'The player with the selected number does not exist.');
        }

        $playerDto = $this->playerMapper->playerEntityToDto($player);
        $view = $this->view($playerDto);

        return $this->handleView($view);
    }

    /**
     * @Route(
     *      "/player",
     *      methods={"GET"}
     * )
     *
     * @ApiDoc(
     *      section="Player Section",
     *      description="Get all players with specific order.",
     *      output={
     *          "class"="AppBundle\Dto\PlayerDto"
     *      }
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $playerList = $this->playerService->listPlayer();
        $playerDtoList = $this->playerMapper->playerEntityListToDtoList($playerList);
        $view = $this->view($playerDtoList);

        return $this->handleView($view);
    }

    /**
     * @Route(
     *      "/player",
     *      methods={"POST"}
     * )
     *
     * @ApiDoc(
     *      section="Player Section",
     *      description="Create new player with input data.",
     *      input={
     *          "class"="AppBundle\Dto\PlayerDto"
     *      },
     *      output={
     *          "class"="AppBundle\Dto\PlayerDto"
     *      }
     * )
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        // Get input and validate if is correct
        $playerDto = $this->playerMapper->playerJsonToDto($request->getContent());
        $errorArray = $this->validationService->validateInput($playerDto);
        if(count($errorArray) > 0) {
            return $this->handleView($this->view($errorArray));
        }

        // If is correct transform input to domain model and do the requested operation
        $player = $this->playerMapper->playerDtoToEntity($playerDto);
        $player = $this->playerService->createPlayer($player);

        // Transform player domain model into dto generating the api rest output model
        $playerDto = $this->playerMapper->playerEntityToDto($player);
        $view = $this->view($playerDto);

        return $this->handleView($view);
    }

}
