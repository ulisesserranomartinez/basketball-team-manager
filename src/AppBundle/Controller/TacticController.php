<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Controller;

use AppBundle\Mapper\TacticMapper;
use AppBundle\Service\TacticService;
use AppBundle\Service\ValidationService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class TacticController
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 *
 * @Route("/api")
 */
class TacticController extends FOSRestController
{

    /**
     * @var TacticService $tacticService
     */
    private $tacticService;

    /**
     * @var TacticMapper
     */
    private $tacticMapper;

    /**
     * @var ValidationService $validationService
     */
    private $validationService;

    /**
     * PlayerController constructor.
     *
     * @param TacticService $tacticService
     * @param TacticMapper $tacticMapper
     * @param ValidationService $validationService
     */
    public function __construct(
        TacticService $tacticService,
        TacticMapper $tacticMapper,
        ValidationService $validationService
    ){
        $this->tacticService = $tacticService;
        $this->tacticMapper = $tacticMapper;
        $this->validationService = $validationService;
    }

    /**
     * @Route(
     *      "/tactic/{tacticId}",
     *      requirements={
     *          "tacticId": "\d+"
     *      },
     *      methods={"GET"}
     * )
     *
     * @ApiDoc(
     *      section="Tactic Section",
     *      description="Get tactic with specific id.",
     *      output={
     *          "class"="AppBundle\Dto\TacticDto"
     *      }
     * )
     *
     * @param int $tacticId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \AppBundle\Exception\TacticNotFoundException
     */
    public function getAction(int $tacticId)
    {
        $tactic = $this->tacticService->findTacticById($tacticId);

        if(!$tactic) {
            throw new HttpException(400, 'The tactic with the selected id does not exist.');
        }

        $tacticDto = $this->tacticMapper->tacticEntityToDto($tactic);
        $view = $this->view($tacticDto);

        return $this->handleView($view);
    }

    /**
     * @Route(
     *      "/tactic",
     *      methods={"GET"}
     * )
     *
     * @ApiDoc(
     *      section="Tactic Section",
     *      description="Get all tactics.",
     *      output={
     *          "class"="AppBundle\Dto\TacticDto"
     *      }
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $tacticList = $this->tacticService->listTactic();
        $tacticDtoList = $this->tacticMapper->tacticEntityListToDtoList($tacticList);
        $view = $this->view($tacticDtoList);

        return $this->handleView($view);
    }

    /**
     * @Route(
     *      "/tactic",
     *      methods={"POST"}
     * )
     *
     * @ApiDoc(
     *      section="Tactic Section",
     *      description="Create new tactic with input data.",
     *      input={
     *          "class"="AppBundle\Dto\TacticDto"
     *      },
     *      output={
     *          "class"="AppBundle\Dto\TacticDto"
     *      }
     * )
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        // Get input and validate if is correct
        $tacticDto = $this->tacticMapper->tacticJsonToDto($request->getContent());
        $errorArray = $this->validationService->validateInput($tacticDto);
        if(count($errorArray) > 0) {
            return $this->handleView($this->view($errorArray));
        }

        // If is correct transform input to domain model and do the requested operation
        $tactic = $this->tacticMapper->tacticDtoToEntity($tacticDto);
        $tactic = $this->tacticService->createTactic($tactic);

        // Transform player domain model into dto generating the api rest output model
        $tacticDto = $this->tacticMapper->tacticEntityToDto($tactic);
        $view = $this->view($tacticDto);

        return $this->handleView($view);
    }

}
