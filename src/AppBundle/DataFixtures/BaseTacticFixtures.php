<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Tactic;
use AppBundle\Util\Roles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class BaseTacticFixtures
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class BaseTacticFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $tactic = $this->createTacticDefend131();
        $manager->persist($tactic);

        $tactic = $this->createTacticZone23();
        $manager->persist($tactic);

        $tactic = $this->createTacticAtack221();
        $manager->persist($tactic);

        $manager->flush();
    }

    private function createTacticDefend131()
    {
        $tactic = new Tactic();
        $tactic
            ->setName('Defensa 1-3-1')
            ->setBaseTactic(true)
            ->setRole1(Roles::BASE)
            ->setRole2(Roles::ESCOLTA)
            ->setRole3(Roles::ESCOLTA)
            ->setRole4(Roles::ALAPIVOT)
            ->setRole5(Roles::PIVOT);

        return $tactic;
    }

    private function createTacticZone23()
    {
        $tactic = new Tactic();
        $tactic
            ->setName('Defensa Zonal 2-3')
            ->setBaseTactic(true)
            ->setRole1(Roles::BASE)
            ->setRole2(Roles::BASE)
            ->setRole3(Roles::ALERO)
            ->setRole4(Roles::PIVOT)
            ->setRole5(Roles::ALAPIVOT);

        return $tactic;
    }

    private function createTacticAtack221()
    {
        $tactic = new Tactic();
        $tactic
            ->setName('Ataque 2-2-1')
            ->setBaseTactic(true)
            ->setRole1(Roles::BASE)
            ->setRole2(Roles::ALERO)
            ->setRole3(Roles::ESCOLTA)
            ->setRole4(Roles::PIVOT)
            ->setRole5(Roles::ALAPIVOT);

        return $tactic;
    }
}
