<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Player;
use AppBundle\Util\Roles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class PlayerFixtures
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class PlayerFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $rolesArr = Roles::toArrayValues();

        for ($i = 0; $i < 20; $i++) {
            $player = new Player();
            $player
                ->setName("Player $i")
                ->setNumber($i)
                ->setRole($rolesArr[mt_rand(0, count($rolesArr) - 1)])
                ->setScore(mt_rand(0, 100));

            $manager->persist($player);
        }

        $manager->flush();
    }

}
