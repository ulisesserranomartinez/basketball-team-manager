<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Tactic;
use AppBundle\Util\Roles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class TacticFixtures
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TacticFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $rolesArr = Roles::toArrayValues();

        for ($i = 0; $i < 20; $i++) {
            $tactic = new Tactic();
            $tactic
                ->setName("Tactic $i")
                ->setBaseTactic(false)
                ->setRole1($rolesArr[mt_rand(0, count($rolesArr) - 1)])
                ->setRole2($rolesArr[mt_rand(0, count($rolesArr) - 1)])
                ->setRole3($rolesArr[mt_rand(0, count($rolesArr) - 1)])
                ->setRole4($rolesArr[mt_rand(0, count($rolesArr) - 1)])
                ->setRole5($rolesArr[mt_rand(0, count($rolesArr) - 1)]);

            $manager->persist($tactic);
        }

        $manager->flush();
    }

}
