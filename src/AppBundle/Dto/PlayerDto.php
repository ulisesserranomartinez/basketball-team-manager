<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class PlayerDto
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class PlayerDto
{

    /**
     * @var integer|null
     *
     * @Assert\Type("integer")
     *
     * @Serializer\Type("integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\GreaterThan(0)
     *
     * @Serializer\Type("integer")
     */
    private $number;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"AppBundle\Util\Roles", "toArray"})
     *
     * @Serializer\Type("string")
     */
    private $role;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 0,
     *      max = 100
     * )
     *
     * @Serializer\Type("integer")
     */
    private $score;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return PlayerDto
     */
    public function setId(?int $id): PlayerDto
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @param int $number
     * @return PlayerDto
     */
    public function setNumber(int $number): PlayerDto
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return PlayerDto
     */
    public function setName(string $name): PlayerDto
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return PlayerDto
     */
    public function setRole(string $role): PlayerDto
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @param int $score
     * @return PlayerDto
     */
    public function setScore(int $score): PlayerDto
    {
        $this->score = $score;

        return $this;
    }

}
