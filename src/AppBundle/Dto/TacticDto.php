<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class TacticDto
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TacticDto
{

    /**
     * @var integer|null
     *
     * @Assert\Type("integer")
     *
     * @Serializer\Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"AppBundle\Util\Roles", "toArray"})
     *
     * @Serializer\Type("string")
     */
    private $role1;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"AppBundle\Util\Roles", "toArray"})
     *
     * @Serializer\Type("string")
     */
    private $role2;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"AppBundle\Util\Roles", "toArray"})
     *
     * @Serializer\Type("string")
     */
    private $role3;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"AppBundle\Util\Roles", "toArray"})
     *
     * @Serializer\Type("string")
     */
    private $role4;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"AppBundle\Util\Roles", "toArray"})
     *
     * @Serializer\Type("string")
     */
    private $role5;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return TacticDto
     */
    public function setId(?int $id): TacticDto
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TacticDto
     */
    public function setName(string $name): TacticDto
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole1(): string
    {
        return $this->role1;
    }

    /**
     * @param string $role1
     * @return TacticDto
     */
    public function setRole1(string $role1): TacticDto
    {
        $this->role1 = $role1;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole2(): string
    {
        return $this->role2;
    }

    /**
     * @param string $role2
     * @return TacticDto
     */
    public function setRole2(string $role2): TacticDto
    {
        $this->role2 = $role2;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole3(): string
    {
        return $this->role3;
    }

    /**
     * @param string $role3
     * @return TacticDto
     */
    public function setRole3(string $role3): TacticDto
    {
        $this->role3 = $role3;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole4(): string
    {
        return $this->role4;
    }

    /**
     * @param string $role4
     * @return TacticDto
     */
    public function setRole4(string $role4): TacticDto
    {
        $this->role4 = $role4;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole5(): string
    {
        return $this->role5;
    }

    /**
     * @param string $role5
     * @return TacticDto
     */
    public function setRole5(string $role5): TacticDto
    {
        $this->role5 = $role5;

        return $this;
    }

}
