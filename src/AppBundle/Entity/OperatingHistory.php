<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

/**
 * Class Player
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 *
 * @Table(name="operating_history")
 * @Entity(repositoryClass="AppBundle\Repository\OperatingHistoryRepository")
 */
class OperatingHistory
{
    /**
     * @var integer|null
     *
     * @Column(type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="text", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @Column(type="text", nullable=false)
     */
    private $operation;

    /**
     * @var string
     *
     * @Column(type="text", nullable=false)
     */
    private $serializedEntity;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", nullable=false)
     */
    private $date;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return OperatingHistory
     */
    public function setId(?int $id): OperatingHistory
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     * @return OperatingHistory
     */
    public function setEntity(string $entity): OperatingHistory
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperation(): string
    {
        return $this->operation;
    }

    /**
     * @param string $operation
     * @return OperatingHistory
     */
    public function setOperation(string $operation): OperatingHistory
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * @return string
     */
    public function getSerializedEntity(): string
    {
        return $this->serializedEntity;
    }

    /**
     * @param string $serializedEntity
     * @return OperatingHistory
     */
    public function setSerializedEntity(string $serializedEntity): OperatingHistory
    {
        $this->serializedEntity = $serializedEntity;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return OperatingHistory
     */
    public function setDate(\DateTime $date): OperatingHistory
    {
        $this->date = $date;

        return $this;
    }

}
