<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

/**
 * Class Tactic
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 *
 * @Table(name="tactic")
 * @Entity(repositoryClass="AppBundle\Repository\TacticRepository")
 */
class Tactic
{
    /**
     * @var integer|null
     *
     * @Column(type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="text", nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @Column(type="boolean", nullable=false)
     */
    private $baseTactic;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false, columnDefinition="enum('BASE', 'ESCOLTA', 'ALERO', 'ALA-PIVOT', 'PIVOT')")
     */
    private $role1;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false, columnDefinition="enum('BASE', 'ESCOLTA', 'ALERO', 'ALA-PIVOT', 'PIVOT')")
     */
    private $role2;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false, columnDefinition="enum('BASE', 'ESCOLTA', 'ALERO', 'ALA-PIVOT', 'PIVOT')")
     */
    private $role3;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false, columnDefinition="enum('BASE', 'ESCOLTA', 'ALERO', 'ALA-PIVOT', 'PIVOT')")
     */
    private $role4;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false, columnDefinition="enum('BASE', 'ESCOLTA', 'ALERO', 'ALA-PIVOT', 'PIVOT')")
     */
    private $role5;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Tactic
     */
    public function setId(?int $id): Tactic
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Tactic
     */
    public function setName(string $name): Tactic
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBaseTactic(): bool
    {
        return $this->baseTactic;
    }

    /**
     * @param bool $baseTactic
     * @return Tactic
     */
    public function setBaseTactic(bool $baseTactic): Tactic
    {
        $this->baseTactic = $baseTactic;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole1(): string
    {
        return $this->role1;
    }

    /**
     * @param string $role1
     * @return Tactic
     */
    public function setRole1(string $role1): Tactic
    {
        $this->role1 = $role1;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole2(): string
    {
        return $this->role2;
    }

    /**
     * @param string $role2
     * @return Tactic
     */
    public function setRole2(string $role2): Tactic
    {
        $this->role2 = $role2;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole3(): string
    {
        return $this->role3;
    }

    /**
     * @param string $role3
     * @return Tactic
     */
    public function setRole3(string $role3): Tactic
    {
        $this->role3 = $role3;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole4(): string
    {
        return $this->role4;
    }

    /**
     * @param string $role4
     * @return Tactic
     */
    public function setRole4(string $role4): Tactic
    {
        $this->role4 = $role4;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole5(): string
    {
        return $this->role5;
    }

    /**
     * @param string $role5
     * @return Tactic
     */
    public function setRole5(string $role5): Tactic
    {
        $this->role5 = $role5;

        return $this;
    }

}
