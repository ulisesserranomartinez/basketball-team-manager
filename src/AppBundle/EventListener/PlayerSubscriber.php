<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\OperatingHistory;
use AppBundle\Entity\Player;
use AppBundle\Mapper\PlayerMapper;
use AppBundle\Util\EntityOperationsEnum;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class PlayerSubscriber
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class PlayerSubscriber implements EventSubscriber
{

    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var PlayerMapper $playerMapper
     */
    private $playerMapper;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * PlayerSubscriber constructor.
     *
     * @param LoggerInterface $logger
     * @param PlayerMapper $playerMapper
     * @param SerializerInterface $serializer
     */
    public function __construct(
        LoggerInterface $logger,
        PlayerMapper $playerMapper,
        SerializerInterface $serializer
    ){
        $this->logger = $logger;
        $this->playerMapper = $playerMapper;
        $this->serializer = $serializer;
    }

    /**
     * Subscriber called before persist doctrine function.
     *
     * @param LifecycleEventArgs $args
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postPersist(LifecycleEventArgs $args)
    {

        $entity = $args->getEntity();

        if ($entity instanceof Player) {
            $playerDto = $this->playerMapper->playerEntityToDto($entity);
            $playerJson = $this->serializer->serialize($playerDto, 'json');

            $operatingHistory = new OperatingHistory();
            $operatingHistory
                ->setEntity(Player::class)
                ->setOperation(EntityOperationsEnum::PERSIST)
                ->setSerializedEntity($playerJson)
                ->setDate(new \DateTime());

            $em = $args->getEntityManager();
            $em->persist($operatingHistory);
            $em->flush();

            $this->logger->info('Persisted entity ' . Player::class . ' with serialized content: ' . $playerJson);
        }
    }

    /**
     * Subscriber called before remove doctrine function.
     *
     * @param LifecycleEventArgs $args
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postRemove(LifecycleEventArgs $args)
    {

        $entity = $args->getEntity();

        if ($entity instanceof Player) {
            $playerDto = $this->playerMapper->playerEntityToDto($entity);
            $playerJson = $this->serializer->serialize($playerDto, 'json');

            $operatingHistory = new OperatingHistory();
            $operatingHistory
                ->setEntity(Player::class)
                ->setOperation(EntityOperationsEnum::REMOVE)
                ->setSerializedEntity($playerJson)
                ->setDate(new \DateTime());

            $em = $args->getEntityManager();
            $em->persist($operatingHistory);
            $em->flush();

            $this->logger->info('Deleted entity ' . Player::class . ' with serialized content: ' . $playerJson);
        }
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postRemove'
        ];
    }

}
