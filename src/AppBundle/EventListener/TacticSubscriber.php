<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\OperatingHistory;
use AppBundle\Entity\Tactic;
use AppBundle\Mapper\PlayerMapper;
use AppBundle\Mapper\TacticMapper;
use AppBundle\Util\EntityOperationsEnum;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class TacticSubscriber
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TacticSubscriber implements EventSubscriber
{

    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var TacticMapper $tacicMapper
     */
    private $tacticMapper;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * TacticSubscriber constructor.
     *
     * @param LoggerInterface $logger
     * @param TacticMapper $tacticMapper
     * @param SerializerInterface $serializer
     */
    public function __construct(
        LoggerInterface $logger,
        TacticMapper $tacticMapper,
        SerializerInterface $serializer
    ){
        $this->logger = $logger;
        $this->tacticMapper = $tacticMapper;
        $this->serializer = $serializer;
    }

    /**
     * Subscriber called before persist doctrine function.
     *
     * @param LifecycleEventArgs $args
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postPersist(LifecycleEventArgs $args)
    {

        $entity = $args->getEntity();

        if ($entity instanceof Tactic) {
            $tacticDto = $this->tacticMapper->tacticEntityToDto($entity);
            $tacticJson = $this->serializer->serialize($tacticDto, 'json');

            $operatingHistory = new OperatingHistory();
            $operatingHistory
                ->setEntity(Tactic::class)
                ->setOperation(EntityOperationsEnum::PERSIST)
                ->setSerializedEntity($tacticJson)
                ->setDate(new \DateTime());

            $em = $args->getEntityManager();
            $em->persist($operatingHistory);
            $em->flush();

            $this->logger->info('Persisted entity ' . Tactic::class . ' with serialized content: ' . $tacticJson);
        }
    }

    /**
     * Subscriber called before remove doctrine function.
     *
     * @param LifecycleEventArgs $args
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postRemove(LifecycleEventArgs $args)
    {

        $entity = $args->getEntity();

        if ($entity instanceof Tactic) {
            $tacticDto = $this->tacticMapper->tacticEntityToDto($entity);
            $tacticJson = $this->serializer->serialize($tacticDto, 'json');

            $operatingHistory = new OperatingHistory();
            $operatingHistory
                ->setEntity(Tactic::class)
                ->setOperation(EntityOperationsEnum::REMOVE)
                ->setSerializedEntity($tacticJson)
                ->setDate(new \DateTime());

            $em = $args->getEntityManager();
            $em->persist($operatingHistory);
            $em->flush();

            $this->logger->info('Deleted entity ' . Tactic::class . ' with serialized content: ' . $tacticJson);
        }
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postRemove'
        ];
    }

}
