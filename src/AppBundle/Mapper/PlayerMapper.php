<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Mapper;

use AppBundle\Dto\PlayerDto;
use AppBundle\Entity\Player;
use JMS\Serializer\SerializerInterface;

/**
 * Class PlayerMapper
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class PlayerMapper
{

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * DocumentFactory constructor.
     *
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param Player $player
     *
     * @return PlayerDto
     */
    public function playerEntityToDto(Player $player)
    {
        $playerDto = new PlayerDto();
        $playerDto
            ->setId($player->getId())
            ->setNumber($player->getNumber())
            ->setName($player->getName())
            ->setRole($player->getRole())
            ->setScore($player->getScore());

        return $playerDto;
    }

    /**
     * @param PlayerDto $playerDto
     *
     * @return Player
     */
    public function playerDtoToEntity(PlayerDto $playerDto)
    {
        $player = new Player();
        $player
            ->setId($playerDto->getId())
            ->setNumber($playerDto->getNumber())
            ->setName($playerDto->getName())
            ->setRole($playerDto->getRole())
            ->setScore($playerDto->getScore());

        return $player;
    }

    /**
     * @param Player[] $playerList
     *
     * @return PlayerDto[]
     */
    public function playerEntityListToDtoList(array $playerList)
    {
        $playerDocumentList = [];

        foreach($playerList as $player) {
            $playerDocumentList[] = $this->playerEntityToDto($player);
        }

        return $playerDocumentList;
    }

    /**
     * Convert string json to player dto
     *
     * @param string $json
     *
     * @return array|\JMS\Serializer\scalar|mixed|object
     */
    public function playerJsonToDto(string $json)
    {
        return $this->serializer->deserialize($json, PlayerDto::class, 'json');
    }

}
