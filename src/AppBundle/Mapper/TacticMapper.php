<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Mapper;

use AppBundle\Document\TacticDocument;
use AppBundle\Dto\TacticDto;
use AppBundle\Entity\Tactic;
use JMS\Serializer\SerializerInterface;

/**
 * Class TacticMapper
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TacticMapper
{

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * DocumentFactory constructor.
     *
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param Tactic $tactic
     *
     * @return TacticDto
     */
    public function tacticEntityToDto(Tactic $tactic)
    {
        $tacticDto = new TacticDto();
        $tacticDto
            ->setId($tactic->getId())
            ->setName($tactic->getName())
            ->setRole1($tactic->getRole1())
            ->setRole2($tactic->getRole2())
            ->setRole3($tactic->getRole3())
            ->setRole4($tactic->getRole4())
            ->setRole5($tactic->getRole5());

        return $tacticDto;
    }

    /**
     * @param TacticDto $tacticDto
     *
     * @return Tactic
     */
    public function tacticDtoToEntity(TacticDto $tacticDto)
    {
        $tactic = new Tactic();
        $tactic
            ->setId($tacticDto->getId())
            ->setName($tacticDto->getName())
            ->setRole1($tacticDto->getRole1())
            ->setRole2($tacticDto->getRole2())
            ->setRole3($tacticDto->getRole3())
            ->setRole4($tacticDto->getRole4())
            ->setRole5($tacticDto->getRole5());

        return $tactic;
    }

    /**
     * @param Tactic[] $tacticList
     *
     * @return TacticDto[]
     */
    public function tacticEntityListToDtoList(array $tacticList)
    {
        $tacticDocumentList = [];

        foreach($tacticList as $tactic) {
            $tacticDocumentList[] = $this->tacticEntityToDto($tactic);
        }

        return $tacticDocumentList;
    }

    /**
     * Convert string json to tactic dto
     *
     * @param string $json
     *
     * @return TacticDto
     */
    public function tacticJsonToDto(string $json)
    {
        /** @var TacticDto $tacticDto */
        $tacticDto = $this->serializer->deserialize($json, TacticDto::class, 'json');

        return $tacticDto;
    }

}
