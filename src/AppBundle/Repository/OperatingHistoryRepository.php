<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Repository;

use AppBundle\Entity\OperatingHistory;
use Doctrine\ORM\EntityRepository;

/**
 * Class OperatingHistoryRepository
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class OperatingHistoryRepository extends EntityRepository
{

    /**
     * @param OperatingHistory $operatingHistoriy
     */
    public function create(OperatingHistory $operatingHistoriy)
    {
        $this->_em->persist($operatingHistoriy);
        $this->_em->flush();
    }

}
