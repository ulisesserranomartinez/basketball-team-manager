<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Player;
use Doctrine\ORM\EntityRepository;

/**
 * Class PlayerRepository
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class PlayerRepository extends EntityRepository
{

    /**
     * @param Player $player
     *
     * @return Player
     */
    public function create(Player $player)
    {
        $this->_em->persist($player);
        $this->_em->flush();

        return $player;
    }

    /**
     * @param Player $player
     */
    public function delete(Player $player)
    {
        $this->_em->remove($player);
        $this->_em->flush();
    }

    /**
     * @param string $role
     * @param int[] $exceptionsId
     *
     * @return mixed
     */
    public function findOnePlayerByRoleWithIdExceptions(string $role, array $exceptionsId)
    {
        $qb = $this->_em->createQueryBuilder();

        $query = $qb->select('pl')
            ->from('AppBundle:Player', 'pl')
            ->where($qb->expr()->like('pl.role', "'$role'"));

        if(count($exceptionsId) > 0){
            $query->andWhere($qb->expr()->notIn('pl.id', $exceptionsId));
        }

        $query
            ->orderBy('pl.score', 'DESC')
            ->setMaxResults(1)
        ;

        return $query->getQuery()->getOneOrNullResult();
    }
}
