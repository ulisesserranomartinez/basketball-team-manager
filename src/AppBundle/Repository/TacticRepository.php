<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Tactic;
use Doctrine\ORM\EntityRepository;

/**
 * Class TacticRepository
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TacticRepository extends EntityRepository
{

    /**
     * @param Tactic $tactic
     *
     * @return Tactic
     */
    public function create(Tactic $tactic)
    {
        $this->_em->persist($tactic);
        $this->_em->flush();

        return $tactic;
    }

    /**
     * @param Tactic $tactic
     */
    public function delete(Tactic $tactic)
    {
        $this->_em->remove($tactic);
        $this->_em->flush();
    }
}
