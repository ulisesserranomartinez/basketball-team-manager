<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Service;

use AppBundle\Entity\Player;
use AppBundle\Exception\PlayerNotFoundException;
use AppBundle\Exception\PlayerWithNumberFoundException;
use AppBundle\Repository\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PlayerService
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class PlayerService
{

    /** @var PlayerRepository $playerRepository */
    private $playerRepository;

    /**
     * PlayerService constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->playerRepository = $em->getRepository('AppBundle:Player');
    }

    /**
     * @param Player $player
     *
     * @return Player
     */
    public function createPlayer(Player $player)
    {
        return $this->playerRepository->create($player);
    }

    /**
     * @param string $role
     * @param int[] $exceptionsId
     *
     * @return mixed
     */
    public function findOnePlayerByRoleWithIdExceptions(string $role, array $exceptionsId)
    {
        return $this->playerRepository->findOnePlayerByRoleWithIdExceptions($role, $exceptionsId);
    }

    /**
     * @param int $playerNumber
     *
     * @throws PlayerNotFoundException
     */
    public function deletePlayerByNumber(int $playerNumber)
    {
        $player = $this->findPlayerByNumberOrException($playerNumber);

        $this->playerRepository->delete($player);
    }

    /**
     * @param array $orderBy
     *
     * @return array
     */
    public function listPlayer(array $orderBy = [])
    {
        return $this->playerRepository->findBy([], $orderBy);
    }

    /**
     * @param int $playerNumber
     *
     * @return null|Player
     */
    public function findPlayerByNumber(int $playerNumber)
    {
        /** @var Player $player */
        $player = $this->playerRepository->findOneBy(['number' => $playerNumber]);

        return $player;
    }

    /**
     * @param int $playerNumber
     *
     * @return null|Player
     * @throws PlayerNotFoundException
     */
    public function findPlayerByNumberOrException(int $playerNumber)
    {
        /** @var Player $player */
        $player = $this->playerRepository->findOneBy(['number' => $playerNumber]);

        if (!$player) {
            throw new PlayerNotFoundException('The player with the selected number does not exist.');
        }

        return $player;
    }

}
