<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Service;

use AppBundle\Entity\Tactic;
use AppBundle\Exception\PlayerWithIdFoundException;
use AppBundle\Exception\PlayerWithNumberFoundException;
use AppBundle\Exception\TacticNotCustomizedException;
use AppBundle\Exception\TacticNotFoundException;
use AppBundle\Exception\TacticWithIdFoundException;
use AppBundle\Mapper\TacticMapper;
use AppBundle\Repository\TacticRepository;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;

/**
 * Class TacticService
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TacticService
{

    /** @var TacticRepository $tacticRepository */
    private $tacticRepository;

    /** @var TacticMapper $tacticMapper */
    private $tacticMapper;

    /** @var SerializerInterface $serializer */
    private $serializer;

    /**
     * PlayerService constructor.
     *
     * @param EntityManagerInterface $em
     * @param TacticMapper $tacticMapper
     * @param SerializerInterface $serializer
     */
    public function __construct(
        EntityManagerInterface $em,
        TacticMapper $tacticMapper,
        SerializerInterface $serializer
    ){
        $this->tacticRepository = $em->getRepository('AppBundle:Tactic');
        $this->tacticMapper = $tacticMapper;
        $this->serializer = $serializer;
    }

    /**
     * @param int $tacticId
     *
     * @return Tactic
     *
     * @throws TacticNotFoundException
     */
    public function findTacticById(int $tacticId)
    {
        /** @var Tactic $tactic */
        $tactic = $this->tacticRepository->find($tacticId);

        if (!$tactic) {
            throw new TacticNotFoundException('The tactic with the selected id does not exist.');
        }

        return $tactic;
    }

    /**
     * @param Tactic $tactic
     *
     * @return Tactic
     */
    public function createTactic(Tactic $tactic)
    {
        $tactic->setBaseTactic(false);

        return $this->tacticRepository->create($tactic);
    }

    /**
     * @param int $tacticId
     *
     * @throws TacticNotFoundException
     * @throws TacticNotCustomizedException
     */
    public function deleteTactic(int $tacticId)
    {

        $tactic = $this->findTacticById($tacticId);

        if ($tactic->isBaseTactic()) {
            throw new TacticNotCustomizedException('The tactic with the selected id is base tactic.');
        }

        $this->tacticRepository->delete($tactic);
    }

    /**
     * @param array $orderBy
     *
     * @return array
     */
    public function listTactic(array $orderBy = [])
    {
        return $this->tacticRepository->findBy([], $orderBy);
    }

    /**
     * @return array
     */
    public function listTacticCustomized()
    {
        return $this->tacticRepository->findBy(['baseTactic' => false]);
    }

    /**
     * @return string
     */
    public function listTacticAsJson()
    {
        $tacticDocumentList = $this->tacticMapper->tacticEntityListToDtoList(
            $this->listTactic()
        );

        return $this->serializer->serialize($tacticDocumentList, 'json');
    }

}
