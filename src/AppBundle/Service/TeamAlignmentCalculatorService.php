<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Service;

use AppBundle\Entity\Player;
use AppBundle\Entity\Tactic;
use AppBundle\Exception\TacticCombinationNotPossibleException;

/**
 * Class TeamAlignmentCalculatorService
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TeamAlignmentCalculatorService
{
    /** @var TacticService $tacticService */
    private $tacticService;

    /** @var PlayerService $playerService */
    private $playerService;

    public function __construct(
        TacticService $tacticService,
        PlayerService $playerService
    ){
        $this->tacticService = $tacticService;
        $this->playerService = $playerService;
    }

    /**
     * @param int $tacticId
     *
     * @return Player[]
     *
     * @throws \AppBundle\Exception\TacticNotFoundException
     * @throws TacticCombinationNotPossibleException
     */
    public function calculateAlignment(int $tacticId)
    {
        /** @var Tactic $tactic */
        $tactic = $this->tacticService->findTacticById($tacticId);

        /** @var Player[] $playerList */
        $playerList = [];

        /** @var int[] $exceptionIdArr */
        $exceptionIdArr = [];

        $playerList[] = $this->findOnePlayerByRoleWithIdExceptions($tactic->getRole1(), $exceptionIdArr);
        $playerList[] = $this->findOnePlayerByRoleWithIdExceptions($tactic->getRole2(), $exceptionIdArr);
        $playerList[] = $this->findOnePlayerByRoleWithIdExceptions($tactic->getRole3(), $exceptionIdArr);
        $playerList[] = $this->findOnePlayerByRoleWithIdExceptions($tactic->getRole4(), $exceptionIdArr);
        $playerList[] = $this->findOnePlayerByRoleWithIdExceptions($tactic->getRole5(), $exceptionIdArr);

        return $playerList;
    }

    /**
     * @param string $role
     * @param array $exceptionIdArr
     *
     * @return Player
     *
     * @throws TacticCombinationNotPossibleException
     */
    private function findOnePlayerByRoleWithIdExceptions(string $role, array &$exceptionIdArr)
    {
        /** @var Player|null $player */
        $player = $this->playerService->findOnePlayerByRoleWithIdExceptions($role, $exceptionIdArr);

        if(!$player) {
            throw new TacticCombinationNotPossibleException(
                'No dispone de los jugadores necesarios para realizar la táctica'
            );
        }

        $exceptionIdArr[] = $player->getId();

        return $player;
    }

}
