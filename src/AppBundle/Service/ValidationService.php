<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Service;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ValidationService
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class ValidationService
{

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ValidatorService constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param $document
     *
     * @return array
     */
    public function validateInput($document)
    {
        $errors = $this->validator->validate($document);

        return $this->getErrorsAsArray($errors);
    }

    /**
     * @param ConstraintViolationListInterface $constraintViolationList
     *
     * @return array
     */
    public function getErrorsAsArray(ConstraintViolationListInterface $constraintViolationList)
    {
        $arrError = [];

        foreach($constraintViolationList as $error) {
            $arrError[] = [
                'param' => $error->getPropertyPath(),
                'error' => $error->getMessage()
            ];
        }

        return $arrError;
    }
}