<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Util;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class ConsoleOutputUtil
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class ConsoleOutputUtil
{
    /**
     * Function to clean console
     *
     * @param OutputInterface $output
     */
    public static function cleanConsole(OutputInterface $output)
    {
        $output->write(sprintf("\033\143"));
    }

    /**
     * Function to wait user input
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param QuestionHelper $questionHelper
     */
    public static function pressAnyKeyToContinue(InputInterface $input, OutputInterface $output, QuestionHelper $questionHelper)
    {
        $question = new ConfirmationQuestion('Presione alguna tecla para continuar ...', false);

        $questionHelper->ask($input, $output, $question);
    }

}
