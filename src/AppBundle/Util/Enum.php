<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Util;

use ReflectionClass;
use Symfony\Component\Lock\Exception\NotSupportedException;

/**
 * Abstract Class Enum
 *
 * Partial extraction from: https://stackoverflow.com/a/17045081
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
abstract class Enum
{

    final private function __construct()
    {
        throw new NotSupportedException();
    }

    final private function __clone()
    {
        throw new NotSupportedException();
    }

    final public static function toArray()
    {
        return (new ReflectionClass(static::class))->getConstants();
    }

    final public static function toArrayValues()
    {
        return array_values((new ReflectionClass(static::class))->getConstants());
    }

    final public static function isValid($value)
    {
        return in_array($value, static::toArray());
    }

}
