<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Util;

/**
 * Class MainMenuOptionsEnum
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class MainMenuOptionsEnum extends Enum
{
    const PLAYER_MENU = 'Menu de jugadores';
    const TACTIC_MENU = 'Menu de tácticas';
    const TEAM_ALIGNMENT_MENU = 'Calculadora de alineaciones';
    const CANCEL = 'Salir de la aplicación';
}
