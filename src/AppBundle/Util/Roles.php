<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Util;

/**
 * Class Roles
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
final class Roles extends Enum
{
    const BASE      = 'BASE';
    const ESCOLTA   = 'ESCOLTA';
    const ALERO     = 'ALERO';
    const ALAPIVOT  = 'ALA-PIVOT';
    const PIVOT     = 'PIVOT';
}
