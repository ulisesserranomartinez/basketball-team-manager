<?php

/*
 * Copyright (c) 2018 Ulises Serrano Martínez - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace AppBundle\Util;

/**
 * Class TacticMenuOptionsEnum
 *
 * @author    Ulises Serrano Martínez (ulisesserranomartinez@gmail.com)
 * @copyright 2018 Ulises Serrano Martínez
 * @since     0.1.0
 * @version   0.1.0
 */
class TacticMenuOptionsEnum extends Enum
{
    const ADD_TACTIC_MENU = 'Añadir táctica';
    const LIST_TACTIC_MENU = 'Listar tácticas';
    const DELETE_TACTIC_MENU = 'Eliminar táctica';
    const CANCEL = 'Cancelar';
}
